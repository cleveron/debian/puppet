puppet (8.5.1-1) unstable; urgency=medium

  * Update upstream.
  * Pass environment to the external node classifier (ENC). (Closes: CLV#2)
  * Make systemd the default provider for all Debian family systems.

 -- Raul Tambre <raul.tambre@clevon.com>  Mon, 11 Mar 2024 18:07:10 +0200

puppet (7.15.0-1) unstable; urgency=medium

  * New upstream release (Closes: #1).

 -- Raul Tambre <raul.tambre@clevon.com>  Mon, 28 Mar 2022 08:20:00 +0300

puppet (7.14.0-1) unstable; urgency=medium

  * Update upstream.
  * Patch to support Ed25519.
  * Use modern gem2deb.

 -- Raul Tambre <raul@tambre.ee>  Sun, 30 Jan 2022 20:41:14 +0200

puppet (7.13.1-2) unstable; urgency=medium

  * Update description for new server packaging.
  * Drop JSON adapter patch as it no longer seems to be necessary for JRuby.
  * Patch for Psych 4.0 support.

 -- Raul Tambre <raul@tambre.ee>  Sun, 02 Jan 2022 18:47:02 +0200

puppet (7.13.1-1) unstable; urgency=medium

  * Update upstream.
  * Bump Standards-Version to 4.6.0, no changes required.
  * Add a watch file.

 -- Raul Tambre <raul@tambre.ee>  Sun, 12 Dec 2021 16:50:01 +0200

puppet (7.7.0-2) unstable; urgency=medium

  * Fix user not being created before dpkg-statoverride.

 -- Raul Tambre <raul@tambre.ee>  Thu, 03 Jun 2021 16:00:08 +0300

puppet (7.7.0-1) unstable; urgency=medium

  * Update upstream.
  * Better hiera.yaml defaults.

 -- Raul Tambre <raul@tambre.ee>  Mon, 31 May 2021 12:13:09 +0300

puppet (7.6.1-5) unstable; urgency=medium

  * Change CA directory to /var/lib/puppet/ca to follow the FHS.
  * Clean the config file as the defaults are appropriate for Debian.

 -- Raul Tambre <raul@tambre.ee>  Tue, 18 May 2021 14:27:31 +0300

puppet (7.6.1-4) unstable; urgency=medium

  * Fix default configdir.

 -- Raul Tambre <raul@tambre.ee>  Mon, 17 May 2021 16:06:06 +0300

puppet (7.6.1-3) unstable; urgency=medium

  * Make ruby-selinux a suggestion.
  * Fix user creation.
  * Fix ruby dependency.
  * Add missing license texts.

 -- Raul Tambre <raul@tambre.ee>  Mon, 17 May 2021 11:10:04 +0300

puppet (7.6.1-2) unstable; urgency=medium

  * Fix dependencies.

 -- Raul Tambre <raul@tambre.ee>  Thu, 29 Apr 2021 09:48:20 +0300

puppet (7.6.1-1) unstable; urgency=medium

  * New re-packaged upstream.

 -- Raul Tambre <raul@tambre.ee>  Wed, 28 Apr 2021 12:00:57 +0300
